// Heavially influenced by nsummer's llvm-demo
// on github

#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Pass.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Transforms/Utils/ModuleUtils.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Support/PrettyStackTrace.h>
#include <llvm/Support/ToolOutputFile.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/Program.h>
#include <llvm/Support/CodeGen.h>
#include <llvm/CodeGen/LinkAllAsmWriterComponents.h>
#include <llvm/CodeGen/LinkAllCodegenComponents.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/Signals.h>
using namespace llvm;
using namespace llvm::sys;
char *filename = "httpd.bc";


namespace installocate {
    struct InstAllocate: public ModulePass {
        static char ID;
	InstAllocate(): ModulePass(ID) {}

	virtual bool runOnModule(Module &M) {
            unsigned long func_id = 0;
            long num_functions = 0;
	    llvm::StringMap<Constant *> StackPointerMap;
	    for (auto &F: M) {
                Function *f = &F;
	        if (f->isDeclaration()) continue;
		if (f->isIntrinsic()) continue; // we don't instrument intrinsic functions, so don't count them
		for (auto &BB: F) {
                    for (auto &I: BB) {
		        if (isa<CallInst>(I)) {
                            Function *func = cast<CallInst>(I).getCalledFunction();
			    if (func == nullptr) continue;
			    if (func->getName() == "malloc" || func->getName() == "_Znwm") {
                                errs() << "Dynamic memory allocation!\n";
			        errs() << func->getName() << "\n";
			        // do stuff
		                SmallVector<Value *, 0> args;
		                IRBuilder<> Builder(I.getNextNode());
		                Function *func = cast<Function>(M.getOrInsertFunction("count_heap", FunctionType::get(Type::getVoidTy(M.getContext()), false)));
                                Builder.CreateCall(func, args);
				
			    }
		        }
		    }
		}
	    }   
	    return true;
	}

    };
    char InstAllocate::ID = 2;
    static RegisterPass<InstAllocate> X("Instrument Allocate", "Instrument Allocate", false, false);
}

static void
saveModule(Module const& m, StringRef filename) {
    std::error_code errc;
    raw_fd_ostream out(filename.data(), errc, sys::fs::F_None);

    if (errc) {
      report_fatal_error("error saving llvm module to '" + filename + "': \n"
                         + errc.message());
    }
    WriteBitcodeToFile(m, out);
}

int main(int argc, char **argv) {

    llvm::sys::PrintStackTraceOnErrorSignal(argv[0]);
    llvm::PrettyStackTraceProgram X(argc, argv);
    llvm_shutdown_obj shutdown;
    SMDiagnostic err;
    LLVMContext context;
    std::unique_ptr<Module> module = parseIRFile(filename, err, context);
    if (!module.get()) {
        errs() << "Error reading bitcode" << filename << "\n";
	err.print(argv[0], errs());
	return -1;
    }
    legacy::PassManager pm;
    pm.add(new installocate::InstAllocate());
    pm.run(*module);
    std::string outFile="instrumented_alloc";
    InitializeAllTargets();
    InitializeAllTargetMCs();
    InitializeAllAsmPrinters();
    InitializeAllAsmParsers();
    cl::AddExtraVersionPrinter(TargetRegistry::printRegisteredTargetsForVersion);


    saveModule(*module, StringRef(outFile + ".bc"));

    return 0;	
}
