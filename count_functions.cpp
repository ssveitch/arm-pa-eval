// Heavially influenced by nsummer's llvm-demo
// on github

#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Pass.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
using namespace llvm;

char *filename = "httpd.bc";

namespace fc {
    struct FunctionCounter: public ModulePass {
        static char ID;
	FunctionCounter(): ModulePass(ID) {}

	virtual bool runOnModule(Module &M) {

            long num_functions = 0;
	    for (auto &F: M) {
                Function *f = &F;
		if (f->isIntrinsic()) continue; // we don't instrument intrinsic functions, so don't count them
		num_functions++;
	    }
	    errs() << "Number of functions in this bitcode file: " << num_functions << "\n";
	    return false; // Pass doesn't change the state of bc, so always return false

	}

    };
    char FunctionCounter::ID = 0;
    static RegisterPass<FunctionCounter> X("FunctionCounter", "Function Counter Pass", false, false);
}



int main(int argc, char **argv) {

//    sys::PrintStackTraceOnErrorSignal(argv[0]);
//    llvm::PrettyStackTraceProgram X(argc, argv);
    llvm_shutdown_obj shutdown;
    SMDiagnostic err;
    LLVMContext context;
    std::unique_ptr<Module> module = parseIRFile(filename, err, context);
    if (!module.get()) {
        errs() << "Error reading bitcode" << filename << "\n";
	err.print(argv[0], errs());
	return -1;
    }
    legacy::PassManager pm;
    pm.add(new fc::FunctionCounter());
    pm.run(*module);
    return 0;	
}
