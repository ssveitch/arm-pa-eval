import csv
import sys
# index by function pointer -> stack pointer -> ret_addr

data = {}
unique_entries = set()
num_entries = 0
print("Starting file parsing")
with open(sys.argv[1], newline='') as csvfile:
    for row in csv.reader(csvfile):
        print(num_entries/ 35056452.0, end="\r")
        func_id = row[0]
        sp = row[1]
        ret_addr = row[2]
        num_entries += 1
        if func_id not in data:
            data[func_id] = dict()
        if sp not in data[func_id]:
            data[func_id][sp] = dict()
        if ret_addr not in data[func_id][sp]:
            data[func_id][sp][ret_addr] = 1
        else:
            data[func_id][sp][ret_addr] += 1
print("")
print("Number of triples read", num_entries)

shrunk_data = {}
unique_pairs = set()
for f_id in data:
    if f_id not in shrunk_data:
        shrunk_data[f_id] = dict()
    for sp in data[f_id]:
        if sp not in shrunk_data[f_id]:
            shrunk_data[f_id][sp] = len(data[f_id][sp])
        unique_pairs.add("{},{}".format(f_id, sp))

print("Number unique pairs: ", len(unique_pairs))
largest = -1
for f_id in shrunk_data:
    for sp in shrunk_data[f_id]:
        if largest < shrunk_data[f_id][sp]:
            largest = shrunk_data[f_id][sp]

print("Largest group of duplicates: ", largest)
print("function_id size: ", len(shrunk_data))
sp_counts = {}
unique_sp = set()
for f_id in shrunk_data:
    for sp in shrunk_data[f_id]:
        if sp not in sp_counts:
            sp_counts[sp] = 1
        else:
            sp_counts[sp] += 1
        unique_sp.add(sp)

print("Unique stack pointers: ", len(unique_sp))
largest = -1
for sp in sp_counts:
    if largest < sp_counts[sp]:
        largest = sp_counts[sp]

print("Largest sp: ", largest)
