// Heavially influenced by nsummer's llvm-demo
// on github
#include <llvm/IR/Operator.h>
#include <llvm/IR/CallSite.h>
#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/IR/Constant.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Pass.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
#include <map>
#include <iterator>
#include <regex>
using namespace llvm;

char *filename = "httpd.bc";

namespace fc {
    struct FunctionCounter: public ModulePass {
        static char ID;
	FunctionCounter(): ModulePass(ID) {}

	virtual bool runOnModule(Module &M) {

            long num_functions = 0;
	    for (auto &F: M) {
                Function *f = &F;
//		if (f->isIntrinsic()) continue; // we don't instrument intrinsic functions, so don't count them
		num_functions++;
	    }
	    errs() << "Number of functions in this bitcode file: " << num_functions << "\n";
	    return false; // Pass doesn't change the state of bc, so always return false

	}

    };
    char FunctionCounter::ID = 0;
    static RegisterPass<FunctionCounter> X("FunctionCounter", "Function Counter Pass", false, false);
}
void buildTypeString(const Type *T, llvm::raw_string_ostream &O) {
  if (T->isPointerTy()) {
    O << "ptr.";
    buildTypeString(T->getPointerElementType(), O);
  } else if (T->isStructTy()) {
    auto structName = dyn_cast<StructType>(T)->getStructName();
    std::regex e("^(\\w+\\.\\w+)(\\.\\w+)?$");
    O << std::regex_replace(structName.str(), e, "$1");
  } else if (T->isArrayTy()) {
    O << "ptr.";
    buildTypeString(T->getArrayElementType(), O);
  } else if (T->isFunctionTy()) {
    auto FuncTy = dyn_cast<FunctionType>(T);
    O << "f.";
    buildTypeString(FuncTy->getReturnType(), O);

    for (auto p = FuncTy->param_begin(); p != FuncTy->param_end(); p++) {
      buildTypeString(*p, O);
    }
  } else if (T->isVectorTy()) {
    O << "vec." << T->getVectorNumElements();
    buildTypeString(T->getVectorElementType(), O);
  } else if (T->isVoidTy()) {
    O << "v";
  } else {
    /* Make sure we've handled all cases we want to */
    assert(T->isIntegerTy() || T->isFloatingPointTy());
    T->print(O);
  }
}

namespace ctc {
    struct CodeTypeCounter: public ModulePass {
        static char ID;
        CodeTypeCounter(): ModulePass(ID) {}

	virtual bool runOnModule(Module &M) {
            long total_types = 0;
            std::map<std::string, long> types;
	    for (auto &F: M) {
	        for (auto &BB: F) {
                    for (auto &I: BB) {
                        auto IOpcode = I.getOpcode();
                        if (IOpcode == Instruction::Store) {
			    auto SI = dyn_cast<StoreInst>(&I);
			    assert(SI != nullptr);
			    handleInstruction(types, total_types, SI->getValueOperand());
			} else if (IOpcode == Instruction::Select) {
                            for (unsigned i = 0, end = I.getNumOperands(); i <end; i++) {
                                handleInstruction(types, total_types, I.getOperand(i));
			    }
			} else if (IOpcode == Instruction::Call) {
                            auto CI = dyn_cast<CallInst>(&I);
			    auto CalledFunction = CI->getCalledFunction();
			    if (CalledFunction != nullptr && CalledFunction->isDeclaration()) {
                                for (auto i = 0U, end = CI->getNumArgOperands(); i < end; i++) {
                                    auto arg = CI->getArgOperand(i);
				    auto argType = arg->getType();
				    if (argType->isPointerTy() && argType->getPointerElementType()->isFunctionTy() && !isa<Constant>(arg)) {
                                        handleInstruction(types, total_types, arg);
				    }
				}
			    } else {
                                for (auto i = 0U, end = CI->getNumArgOperands(); i < end; i++) {
                                    handleInstruction(types, total_types, CI->getArgOperand(i));
				}
			    }
			}

		    }
		}
	    }
	    errs() << "Number of code pointer types in this bitcode file: " << total_types << "\n";
	    long sum = 0;
            for (auto cur = types.begin(); cur != types.end(); cur = std::next(cur)) {
	        //errs() << "Number of type: "<< cur->first  << "  in this bitcode file: " << cur->second << "\n";
		sum += cur->second;
	    }
	    assert(sum == total_types);
	    long max = -1;
	    std::string functype = "";
	    for (auto cur = types.begin(); cur != types.end(); cur = std::next(cur)) {
                if (cur->second > max) {
                     max = cur->second;
		     functype = cur->first;
		}
	    }
	    errs() << "Largest number of types of the form: " << functype << ": " << max << "\n";
	    return false; // Pass doesn't change the state of bc, so always return false

	}
	void handleInstruction(std::map<std::string ,long> &types, long &total_count, Value *V) {
	    auto VTypeInput = isa<BitCastOperator>(V) ? dyn_cast<BitCastOperator>(V)->getSrcTy(): V->getType();
	    auto VInput = isa<BitCastOperator>(V) ? dyn_cast<BitCastOperator>(V)->getOperand(0): V;
	    if (!VTypeInput->isPointerTy() || !isa<Function>(VInput) || dyn_cast<Function>(VInput)->isIntrinsic()) return;
            std::string buf;
            llvm::raw_string_ostream typeIdStr(buf);
            buildTypeString(V->getType(), typeIdStr);
            typeIdStr.flush();
            
	    if (types.find(buf) == types.end()) {
                types[buf] = 0;
	    }
            types[buf]++;
	    total_count++;
           // types.insert(LLVMPrintTypeToString(V->getType()));
	}
    };
    char CodeTypeCounter::ID = 1;
    static RegisterPass<CodeTypeCounter> X("Code Type Counter", "Code Type Counter Pass", false, false);
}
static inline bool isDataPointer(const Type *const type) {
  return type->isPointerTy() && !type->getPointerElementType()->isFunctionTy();
}
namespace dtc {
    struct DataTypeCounter: public ModulePass {
        static char ID;
        DataTypeCounter(): ModulePass(ID) {}

	virtual bool runOnModule(Module &M) {
            long total_types = 0;
            std::map<std::string, long> types;
	    for (auto &F: M) {
	        for (auto &BB: F) {
                    for (auto &I: BB) {
                        auto IOpcode = I.getOpcode();
                        if (IOpcode == Instruction::Store) {
                            auto V = dyn_cast<StoreInst>(&I)->getValueOperand();
			    auto VType = V->getType();

			    if (!isDataPointer(VType)) continue;

			    handleInstruction(types, total_types, V);
			} else if (IOpcode == Instruction::Load) {
                            auto VType = dyn_cast<LoadInst>(&I)->getPointerOperandType()->getPointerElementType();

			    if (!isDataPointer(VType)) continue;
			    handleInstruction(types, total_types, I.getNextNode());
			}
		    }
		}
	    }
	    errs() << "Number of data pointer types in this bitcode file: " << total_types << "\n";
	    long sum = 0;
            for (auto cur = types.begin(); cur != types.end(); cur = std::next(cur)) {
	        //errs() << "Number of type: "<< cur->first  << "  in this bitcode file: " << cur->second << "\n";
		sum += cur->second;
	    }
	    assert(sum == total_types);
	    long max = -1;
	    std::string functype = "";
	    for (auto cur = types.begin(); cur != types.end(); cur = std::next(cur)) {
                if (cur->second > max) {
                     max = cur->second;
		     functype = cur->first;
		}
	    }
	    errs() << "Largest number of types of the form: " << functype << ": " << max << "\n";
	    return false; // Pass doesn't change the state of bc, so always return false

	}
	void handleInstruction(std::map<std::string ,long> &types, long &total_count, Value *V) {
            
	    std::string buf;
            llvm::raw_string_ostream typeIdStr(buf);
            buildTypeString(V->getType(), typeIdStr);
            typeIdStr.flush();
            
	    if (types.find(buf) == types.end()) {
                types[buf] = 0;
	    }
            types[buf]++;
	    total_count++;
           // types.insert(LLVMPrintTypeToString(V->getType()));
	}
    };
    char DataTypeCounter::ID = 2;
    static RegisterPass<DataTypeCounter> X("Data Type Counter", "Data Type Counter Pass", false, false);
}

int main(int argc, char **argv) {

//    sys::PrintStackTraceOnErrorSignal(argv[0]);
//    llvm::PrettyStackTraceProgram X(argc, argv);
    llvm_shutdown_obj shutdown;
    SMDiagnostic err;
    LLVMContext context;
    std::unique_ptr<Module> module = parseIRFile(filename, err, context);
    if (!module.get()) {
        errs() << "Error reading bitcode" << filename << "\n";
	err.print(argv[0], errs());
	return -1;
    }
    legacy::PassManager pm;
    pm.add(new fc::FunctionCounter());
    pm.add(new ctc::CodeTypeCounter());
    pm.add(new dtc::DataTypeCounter());
    pm.run(*module);
    return 0;	
}
