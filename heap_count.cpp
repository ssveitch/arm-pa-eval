#include <mutex>
#include "heap_count.h"
#include <fstream>
#include <iostream>
#include <sys/stat.h>
//std::map<unsigned long, unsigned long> sp_list;
std::mutex __heap_mtx;

extern "C" void count_heap(void) {
    __heap_mtx.lock();
    std::ofstream outdata;
    outdata.open("heap_counts.csv", std::ios_base::app);
    if (!outdata) {
       std::cout << "Error! Could not open file!" << std::endl;
       exit(1);
    }
    outdata << 1 << std::endl;
    outdata.close();
    chmod("heap_counts.csv", S_IWUSR | S_IRUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    __heap_mtx.unlock();
}

