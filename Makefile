.PHONY: all

all: count_functions

count_functions: count_functions.cpp
	clang++-8 -o count_functions `llvm-config-8 --cxxflags --ldflags --system-libs --libs` -Wall -Wextra count_functions.cpp

count_types: count_types.cpp
	clang++-8 -o count_types `llvm-config-8 --cxxflags --ldflags --system-libs --libs` -Wall -Wextra count_types.cpp

instrument_sp: instrument_sp.cpp
	clang++-8 -o instrument_sp `llvm-config-8 --cxxflags --ldflags --system-libs --libs` -Wall -Wextra instrument_sp.cpp

instrument_alloc: instrument_alloc.cpp
	clang++-8 -o instrument_alloc `llvm-config-8 --cxxflags --ldflags --system-libs --libs` -Wall -Wextra instrument_alloc.cpp

httpd_count_functions: count_functions httpd.bc
	./count_functions

httpd_count_types: count_types httpd.bc
	./count_types

httpd_sp: instrument_sp libspcount.a libheapcount.a httpd.bc
	./instrument_sp
	llc-8 -filetype=obj instrumented_sp.bc
	clang++-8 -O0 -o httpd instrumented_sp.o -Wl,--export-dynamic -L. -lspcount -lheapcount -lapr-1 -laprutil-1 -lpcre -lpthread

httpd_alloc: instrument_alloc libspcount.a libheapcount.a httpd.bc
	./instrument_alloc
	llc-8 -filetype=obj instrumented_alloc.bc
	clang++-8 -O0 -o httpd instrumented_alloc.o -Wl,--export-dynamic -L. -lspcount -lheapcount -lapr-1 -laprutil-1 -lpcre -lpthread

libspcount.a: sp_count.cpp
	clang++-8 -g -O0 -c -o libspcount.o sp_count.cpp
	ar -crs libspcount.a libspcount.o

libheapcount.a: heap_count.cpp
	clang++-8 -g -O0 -c -o libheapcount.o heap_count.cpp
	ar -crs libheapcount.a libheapcount.o

httpd.bc: build/builddone
	extract-bc -l `which llvm-link-8` build/httpd-build/bin/httpd
	mv build/httpd-build/bin/httpd.bc .

build/builddone:
	mkdir -p build
	./compile-apache-wllvm.sh
	touch build/builddone

clean:
	rm -rf *.o instrumented_*.bc *.a count_functions count_types httpd instrument_sp instrument_alloc httpd.bc build/
