#include <map>
#include <mutex>
#include "sp_count.h"
#include <fstream>
#include <iostream>
#include <sys/stat.h>
//std::map<unsigned long, unsigned long> sp_list;
std::mutex __sp_mtx;
unsigned long get_sp(void) {
    void *p;
    //register void *sp asm("sp");
    //return (unsigned long)sp;
    return (unsigned long)&p;
}

extern "C" void insert_sp(unsigned long function_id, char *ret_addr) {
    unsigned long sp = get_sp();
    __sp_mtx.lock();
    std::ofstream outdata;
    outdata.open("function-id_sp_ret_addr.csv", std::ios_base::app);
    if (!outdata) {
       std::cout << "Error! Could not open file!" << std::endl;
       exit(1);
    }
    outdata << function_id << "," << sp << ","  << (unsigned long)ret_addr  << std::endl;
    outdata.close();
    chmod("function-id_sp_ret_addr.csv", S_IWUSR | S_IRUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
    __sp_mtx.unlock();
}


extern "C" unsigned long num_sp(void) {
    unsigned long sum = 0;
    /*mtx.lock();
    
    for (auto cur = sp_list.begin(); cur != sp_list.end(); cur = std::next(cur)) {
        sum += cur->second;
    }
    mtx.unlock();
    */
    return sum;
}

extern "C" void print_collision_counts(void) {
/*    printf("Size of map: %lu\n", sp_list.size());
    printf("Number of stack pointers witnessed:%lu\n", num_sp());
    unsigned long max = 0;
    unsigned long max_value = 0;
    //mtx.lock();
    for (auto cur_sp = sp_list.begin(); cur_sp != sp_list.end(); cur_sp = std::next(cur_sp)) {
        if (cur_sp->second > max) {
            max = cur_sp->second;
	    max_value = cur_sp->first;
	}
	printf("0x%lx was witnessed %lu times\n", cur_sp->first, cur_sp->second);
    }
    //mtx.unlock();
    printf("The most witnessed sp was 0x%lx, and it was witnessed %lu times\n", max_value, max);
  */  
}
