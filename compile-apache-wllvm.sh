#!/bin/bash

export BUILD_DIR=`pwd`/build
mkdir -p build/
echo "apache will be built in $BUILD_DIR"
cd $BUILD_DIR
mkdir -p httpd
cd httpd
wget "https://mirror.csclub.uwaterloo.ca/apache/httpd/httpd-2.4.46.tar.gz"
tar -xvf "httpd-2.4.46.tar.gz"
mkdir apr
cd apr
wget "https://mirror.dsrg.utoronto.ca/apache/apr/apr-1.7.0.tar.gz"
wget "https://mirror.dsrg.utoronto.ca/apache/apr/apr-util-1.6.1.tar.gz"
tar -xvf "apr-1.7.0.tar.gz"
tar -xvf "apr-util-1.6.1.tar.gz"
cp -r "apr-1.7.0" "../httpd-2.4.46/srclib/apr"
cp -r "apr-util-1.6.1" "../httpd-2.4.46/srclib/apr-util"
cd "../httpd-2.4.46"
mkdir -p "../httpd-build"
export LLVM_COMPILER=clang
export LLVM_CC_NAME=clang-8
export LLVM_CXX_NAME=clang++-8
export LLVM_LINK_NAME=llvm-link-8
export LLVM_AR_NAME=llvm-ar-8
	    #--with-included-apr \
CC=wllvm ./configure --prefix=$BUILD_DIR"/httpd-build" \
	    ac_vc_func_setpgrp_void="no" \
	    ap_cv_void_ptr_lt_long="8" \
	    ac_cv_file__dev_zero="yes" \
	    ac_cv_func_setpgrp_void="yes" \
	    apr_cv_process_shared_works="yes" \
	    apr_cv_mutex_robust_shared="no" \
	    apr_cv_tcp_nodelay_with_cork="yes" \
	    ac_cv_sizeof_struct_iovec="8" \
	    apr_cv_mutext_recursive="yes" \
	    apr_cv_epoll="yes" \
	    apr_cv_epoll_create1="yes" \
	    apr_cv_dup3="yes" \
	    apr_cv_sock_cloexec="yes" \
	    apr_cv_accept4="yes" \
	    --enable-mpms-shared=all \
	    --with-mpm=event
make
make install
