# Evaluating ARM Pointer Authentication

This repo contains the llvm passes written to collect run-time and compile-time information from Apache's httpd.

## Description
Prior to attempting to build, please run `./check_dependencies.sh` to ensure you have the proper dependencies to build apache.

Once all the dependencies are installed, `make` can be used to build one of the llvm passes as well as apache. Note: For the sake of clarity, this repo has been broken up so each pass is in its own file, and only a single pass can be run at a time with make.

There are X passes:
- instrument httpd to collect stack pointer information (sp)
- instrument httpd to collect heap memory allocation (alloc)
- analyze bitcode file to count the number of functions (count_functions)
- analyze bitcode file to count the number of types (count_types)

Note: A lot of warnings will be generated, this is expected.

## Analysis Passes
In order to run one of the analysis passes, enter the command `make httpd_$ANALYSIS NAME` where `$ANLYSISNAME` is either `count_functions` or `count_types`. The results will be output to standard out.

For example, if you wanted to count the number of functions in httpd, run the command `make httpd_count_functions`

# Transformation Passes
In order to instrument httpd to collect run-time information on apache, enter the command `make httpd_$RTTYPE` where `$RTTYPE` is either `sp` pr `alloc`
Then you need to run httpd. We recommend using the `-X` debugging command so that httpd doesn't deattached from your terminal. You can run httpd with `./httpd -X`.

For example, if you wanted to record the number of heap allocations that occur, run the command `make httpd_alloc` followed by `sudo ./httpd -X`. Exit httpd when you are done with your evaluation.

Once you stop httpd, a new file will be written in your current working directory. This file contains the values seen during runtime.
- `heap_counts.csv` coresponds to the output from counting heap allocations. Each line is a '1', so the number of allocations that have occured is equal to the number of lines in the file
- `function-id_sp_ret_addr.csv` corresponds to the output from recording function-id, stack pointer, return address triples. WARNING THIS FILE GETS BIG FAST

Note: It is important to delete the csv files between runs as the the run-time library only appends to these files.

## Analysis Script
For parsing the function-id, stack pointer, return address triples, use the python script `sp_analysis.py`.
