// Heavially influenced by nsummer's llvm-demo
// on github

#include <llvm/IR/Metadata.h>
#include <llvm/IR/Module.h>
#include <llvm/IRReader/IRReader.h>
#include <llvm/Pass.h>
#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/InlineAsm.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Transforms/Utils/ModuleUtils.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Support/PrettyStackTrace.h>
#include <llvm/Support/ToolOutputFile.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/Support/TargetRegistry.h>
#include <llvm/Support/Program.h>
#include <llvm/Support/CodeGen.h>
#include <llvm/CodeGen/LinkAllAsmWriterComponents.h>
#include <llvm/CodeGen/LinkAllCodegenComponents.h>
#include <llvm/Bitcode/BitcodeWriter.h>
#include <llvm/Support/Signals.h>
using namespace llvm;
using namespace llvm::sys;
char *filename = "httpd.bc";



namespace instsp {
    struct Instsp: public ModulePass {
        static char ID;
	Instsp(): ModulePass(ID) {}

	virtual bool runOnModule(Module &M) {
            unsigned long func_id = 0;
            long num_functions = 0;
	    llvm::StringMap<Constant *> StackPointerMap;
	    for (auto &F: M) {
                Function *f = &F;
	        if (f->isDeclaration()) continue;
		if (f->isIntrinsic()) continue; // we don't instrument intrinsic functions, so don't count them
		//TODO give function unique id in args

		IRBuilder<> Builder(&*f->getEntryBlock().getFirstInsertionPt());
		SmallVector<Value *, 1> args0;
                args0.push_back(ConstantInt::get(Type::getInt32Ty(M.getContext()), 0, true));
		std::vector<Type *>argType;
//		argType.push_back(Type::getInt32Ty(M.getContext()));

		Function *ret_addr = cast<Function>(Intrinsic::getDeclaration(&M, Intrinsic::returnaddress, argType));
		CallInst *ret_addr_return = Builder.CreateCall(ret_addr, args0);
		/*
		Type *t = ret_addr_return_dumb->getType();
		if (t->isPointerTy()) {
	            Type *tt = cast<PointerType>(*t).getElementType();
		    if (tt->isIntegerTy())
                    	errs() << cast<IntegerType>(*tt).getBitWidth() << "\n";
		}*/
                //Value *ret_addr_return = ConstantInt::get(Type::getInt8Ty(M.getContext()), 0, true);
		std::vector<Type *> argTypes;
		argTypes.push_back(Type::getInt64Ty(M.getContext()));
		argTypes.push_back(Type::getInt8PtrTy(M.getContext()));
		SmallVector<Value *, 2> args1;
                args1.push_back(ConstantInt::get(Type::getInt64Ty(M.getContext()), func_id, true));
		args1.push_back(ret_addr_return);
		Function *func = cast<Function>(M.getOrInsertFunction("insert_sp", FunctionType::get(Type::getVoidTy(M.getContext()),argTypes, false)));
                Builder.CreateCall(func, args1);
	        func_id++; 
	
	    }   
            auto func = M.getOrInsertFunction("print_collision_counts", FunctionType::get(Type::getVoidTy(M.getContext()), false));		
            appendToGlobalDtors(M, llvm::cast<Function>(func), 0);
	    return true;
	}

    };
    char Instsp::ID = 1;
    static RegisterPass<Instsp> X("Instrument SP", "Instrument SP", false, false);
}


static void
saveModule(Module const& m, StringRef filename) {
    std::error_code errc;
    raw_fd_ostream out(filename.data(), errc, sys::fs::F_None);

    if (errc) {
      report_fatal_error("error saving llvm module to '" + filename + "': \n"
                         + errc.message());
    }
    WriteBitcodeToFile(m, out);
}

int main(int argc, char **argv) {

    llvm::sys::PrintStackTraceOnErrorSignal(argv[0]);
    llvm::PrettyStackTraceProgram X(argc, argv);
    llvm_shutdown_obj shutdown;
    SMDiagnostic err;
    LLVMContext context;
    std::unique_ptr<Module> module = parseIRFile(filename, err, context);
    if (!module.get()) {
        errs() << "Error reading bitcode" << filename << "\n";
	err.print(argv[0], errs());
	return -1;
    }
    legacy::PassManager pm;
    pm.add(new instsp::Instsp());
    pm.run(*module);
    std::string outFile="instrumented_sp";
    InitializeAllTargets();
    InitializeAllTargetMCs();
    InitializeAllAsmPrinters();
    InitializeAllAsmParsers();
    cl::AddExtraVersionPrinter(TargetRegistry::printRegisteredTargetsForVersion);


    saveModule(*module, StringRef(outFile + ".bc"));

    return 0;	
}
