#!/bin/bash

# check for needed commands
which clang-8 > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install clang-8"
fi
which clang++-8 > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install clang++-8"
fi
which llvm-config-8 > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install llvm-8 and llvm-8-dev"
fi
which wllvm > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install wllvm"
fi

ldconfig -p | egrep libpcre3 > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install libpcre3-dev"
fi
ldconfig -p | egrep libapr-1 > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install libapr1-dev"
fi
ldconfig -p | egrep libaprutil-1 > /dev/null
if [[ $? != 0 ]]
then
	echo "Please install libaprutil1-dev"
fi
